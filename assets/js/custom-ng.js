var blog = angular.module("myBlog", ['ngRoute', 'UserValidation']);
blog.controller('userCtrl', function($scope) {
    $scope.fullName = '';
    $scope.login = '';
    $scope.passw1 = '';
    $scope.passw2 = '';
    $scope.clearForm = function () {
        $scope.fullName = '';
        $scope.login = '';
        $scope.passw1 = '';
        $scope.passw2 = '';
        $('#registerModal').modal('hide');

    };

        $scope.clearLoginForm = function () {
            $scope.login = '';
            $scope.passw1 = '';
            $('#loginModal').modal('hide');

        };
});
// blog.controller('showMe', function($scope) {
//     $scope.showMe = false;
//        $scope.myFunc = function() {
//            $scope.showMe = !$scope.showMe;
//        }

    //    $scope.doSomethingElse = function(){
    //      //some cool things happen here, and:
    //      $scope.showFirstButton = !$scope.showFirstButton
    //    }
// });


// 
// blog.controller('customersCtrl', function signIn($scope, $http, $log) {
//     var obj = {};
// 	obj.user_login = document.getElementById('user_login').value;
// 	obj.user_password = document.getElementById('user_password').value;
//
//     $http.post('http://localhost:3000/authorisation', obj)
//     .then(function (response) {
//         $scope.names = response.data;
//         console.log(data);
//         $log.info(response);
//     });
//
// });


angular.module('UserValidation', []).directive('wjValidationError', function () {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctl) {
      scope.$watch(attrs['wjValidationError'], function (errorMsg) {
        elm[0].setCustomValidity(errorMsg);
        ctl.$setValidity('wjValidationError', errorMsg ? false : true);
      });
    }
  };
});

blog.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true
    })
    $routeProvider

    // .when('/', {
    //     templateUrl: 'index.html',
    //     controller: 'mainNewsCtrl'
    // })
    .when('/login', {
        templateUrl: 'template/login.html',
        controller: 'loginCtrl'
    })
    .when('/registration', {
        templateUrl: 'template/register.html',
        controller: 'registrationCtrl'
    })
    .when('/dashboard', {
        templateUrl: 'dashboard.html'
        // controller: 'loginCtrl'
    })
    .otherwise({
        redirectTo: '/index.html'
    });
}]);
blog.controller('registrationCtrl', ['$scope',  '$location', function($scope, $location) {
    $scope.regSubmit = function() {

            $location.path('/');

    }
}]);
blog.controller('mainNewsCtrl', ['$scope',  '$location', function($scope, $location) {
$location.path('/');
}]);


blog.controller('loginCtrl', function($scope, $location){
    $scope.submit = function() {

            $location.path('/dashboard');

    }
});
